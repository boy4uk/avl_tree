﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AVL_tree
{
    public class Program
    {
        public static void Main()
        {
            var tree = new AVL<int>();
            tree.Add(1);
            tree.Add(2);
            tree.Add(3);
            tree.Add(6);
            tree.Add(1);
            tree.Add(0);

        }
    }


    public class AVL<T>
        where T : IComparable
    {
        public Node<T> Root { get; set; }

        public class Node<T>
        {
            public T Value { get; set; }
            public Node<T> Left { get; set; }
            public Node<T> Right { get; set; }
            public Node<T> Parent { get; set; }
            public int Balance { get; set; }

            public Node(T Value = default(T)) => this.Value = Value;
        }

        public void Add(T inputValue)
        {
            var current = Root;
            var parent = new Node<T>();

            if (Root == null)
            {
                Root = new Node<T>(inputValue);
                return;
            }
            while (current != null)
            {
                parent = current;
                current = inputValue.CompareTo(current.Value) < 0 ? current.Left : current.Right;
            }
            if (inputValue.CompareTo(parent.Value) < 0)
            {
                parent.Left = new Node<T>(inputValue) { Parent = parent };
                Balance(parent.Left.Parent, 1);
            }
            else
            {
                parent.Right = new Node<T>(inputValue) { Parent = parent };
                Balance(parent.Right.Parent, -1);
            }

        }

        private void Balance(Node<T> node, int balance)
        {
            while (node != null)
            {
                balance = (node.Balance += balance);

                if (balance == 0)
                    return;

                if (balance == 2)
                {
                    if (node.Left.Balance == 1)
                        RotateRight(node);
                    else
                        RotateLeftRight(node);
                    return;
                }

                if (balance == -2)
                {
                    if (node.Right.Balance == -1)
                        RotateLeft(node);
                    else
                        RotateRightLeft(node);
                    return;
                }

                if (node.Parent != null)
                    balance = node.Parent.Left == node ? 1 : -1;
                node = node.Parent;
            }
        }

        private Node<T> RotateLeft(Node<T> node)
        {

            var right = node.Right;
            var rightLeft = right.Left;
            var parent = node.Parent;

            right.Parent = parent;
            right.Left = node;
            node.Right = rightLeft;
            node.Parent = right;
            
            //То что выше выглядит так себе, но зато сам код более читаемый. Без этих вещей я запутывался.
            //С другими поворотами ситуация похожая

            if (rightLeft != null)
                rightLeft.Parent = node;

            if (node == Root)
                Root = right;
            else 
            if (parent.Right == node)
                parent.Right = right;
            else
                parent.Left = right;

            right.Balance++;
            node.Balance = -right.Balance;

            return right;
        }

        private Node<T> RotateRight(Node<T> node)
        {
            var left = node.Left;
            var leftRight = left.Right;
            var parent = node.Parent;

            left.Parent = parent;
            left.Right = node;
            node.Left = leftRight;
            node.Parent = left;

            if (leftRight != null)
                leftRight.Parent = node;

            if (node == Root)
                Root = left;
            else if (parent.Left == node)
                parent.Left = left;
            else
                parent.Right = left;

            left.Balance--;
            node.Balance = -left.Balance;

            return left;
        }

        private Node<T> RotateLeftRight(Node<T> node)
        {
            var left = node.Left;
            var leftRight = left.Right;
            var parent = node.Parent;
            var leftRightRight = leftRight.Right;
            var leftRightLeft = leftRight.Left;

            leftRight.Parent = parent;
            node.Left = leftRightRight;
            left.Right = leftRightLeft;
            leftRight.Left = left;
            leftRight.Right = node;
            left.Parent = leftRight;
            node.Parent = leftRight;

            if (leftRightRight != null)
                leftRightRight.Parent = node;

            if (leftRightLeft != null)
                leftRightLeft.Parent = left;

            if (node == Root)
                Root = leftRight;
            else if (parent.Left == node)
                parent.Left = leftRight;
            else
                parent.Right = leftRight;

            if (leftRight.Balance == -1)
            {
                node.Balance = 0;
                left.Balance = 1;
            }
            else if (leftRight.Balance == 0)
            {
                node.Balance = 0;
                left.Balance = 0;
            }
            else
            {
                node.Balance = -1;
                left.Balance = 0;
            }

            leftRight.Balance = 0;

            return leftRight;
        }

        private Node<T> RotateRightLeft(Node<T> node)
        {
            var right = node.Right;
            var rightLeft = right.Left;
            var parent = node.Parent;
            var rightLeftLeft = rightLeft.Left;
            var rightLeftRight = rightLeft.Right;

            rightLeft.Parent = parent;
            node.Right = rightLeftLeft;
            right.Left = rightLeftRight;
            rightLeft.Right = right;
            rightLeft.Left = node;
            right.Parent = rightLeft;
            node.Parent = rightLeft;

            if (rightLeftLeft != null)
                rightLeftLeft.Parent = node;

            if (rightLeftRight != null)
                rightLeftRight.Parent = right;

            if (node == Root)
                Root = rightLeft;
            else if (parent.Right == node)
                parent.Right = rightLeft;
            else
                parent.Left = rightLeft;

            if (rightLeft.Balance == 1)
            {
                node.Balance = 0;
                right.Balance = -1;
            }
            else if (rightLeft.Balance == 0)
            {
                node.Balance = 0;
                right.Balance = 0;
            }
            else
            {
                node.Balance = 1;
                right.Balance = 0;
            }

            rightLeft.Balance = 0;

            return rightLeft;
        }

        public void Clear() => Root = null;

    }
}
